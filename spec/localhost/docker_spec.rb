require 'spec_helper'

describe docker_image('hireme:latest') do
  it { should exist }
end

describe docker_container('hireme') do
  it { should exist }
end

describe docker_container('hireme') do
  it { should be_running }
end

describe port(80) do
  it { should be_listening }
end