Vagrant Docker Ansible Integration
==============

## Getting started ##
### Prerequisites ###
There are a couple dependencies you must have installed on your host machine to be able to successfully start this vagrant box. The tools, along with links to their product page are defined below:

* [Vagrant](https://www.vagrantup.com/)
* [Virtual Box](https://www.virtualbox.org/)

### Starting the vagrant box ###

You can start this vagrant box by navigating to your local cloned repository and calling the `vagrant up` command. An example is shown below


~~~~
$ vagrant up
~~~~


### Browsing to the static content ###
The vagrant box will display content at the IP address `192.168.33.10` on the host machine.

~~~~
http://192.168.33.10/
~~~~

## Additional useful commands ##

~~~~
$ vagrant ssh
~~~~

~~~~
$ vagrant halt
~~~~

~~~~
$ vagrant destroy
~~~~

Please refer to the official Vagrant documentation for further details around the vagrant cli. [https://www.vagrantup.com/docs/](https://www.vagrantup.com/docs/)
